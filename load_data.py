import argparse
import os
from src.lib import exec_engine

def load(path):
    engine=exec_engine.ExecEngine()
    if dir:
        files = (file for file in os.listdir(path)
                 if os.path.isfile(os.path.join(path, file)))
        for file_path in files:
            engine.load_data(path+"/"+file_path)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("--dir", help="directory", default='test/test_data')
	args = parser.parse_args()
	args = vars(args)
	load(args['dir'])