import argparse
from src import run_server

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("--port", help="Port Number", default=8080)
	args = parser.parse_args()
	args = vars(args)
	run_server.run_server(int(args['port']))