# testendpoints

# Directory Structure
1. Code:: src
2. Test:: test
3. Configurations:: config
4. Makefile :: base directory
5. run_log.log: logs execution information
6. error_log.log: logs error information


# Environment Variables
*DB Parameters*
DB_USER (default=root)
DB_PASSWORD (default=)
DB_DBNAME (default=TEST_SCHEMA)
DB_HOST (default=localhost)
   
# Execution Instructions
1. To Install: make install
2. To run local server: make start-server
3. To stop local server: make stop-server
4. Smoke test client: venv/bin/python -m unittest test.test_endpoint
5. Load data: venv/bin/python load_data.py -dir=<path>
6. Run any tests: venv/bin/python -m unittest test/test_<harness_name>

# End Points
1. url/query_gene, expecting post form (gene=<string>, start_limit=<integer>, end_limit=<integer>)
2. url/query_hgvs, expecting post form (hgvs=<string>, start_limit=<integer>, end_limit=<integer>)

# Assumptions
1. Querying Assumes case independence

# Technology used
1. Webserver: Flask based
2. Database: Monolithic MySQL community edition
3. Testing: python unittest

# Performance considerations
1. Indexes built on gene and hgvs keys

# Basic Flow Idea
1. Create Schema in MySQL
2. Read .RSV file and covert into dict (Gene, HGVS, JSON_BLOB)
3. Load data into Database as (Gene - column , HGVS - column , JSON_BLOB column)
4. Indexes are built on top of Gene and HGVS
5. Testing is done on top of API to load and query data
6. Webserver started via flask using built in api
7. local end-points via Post Method:
    1) for Gene Query: http://127.0.0.1:8080/query_gene 
        params (gene, start_limit, end_limit) 
    2) for HGVS:       http://127.0.0.1:8080/query_hgvs
        params (hgvs, start_limit, end_limit) 
We use start_limit, end_limit for pagination for performance constraints

# Testing Strategy
1. Functionality
2. Load Test
3. End to End Test
   - Client based
   - API based

# Requirements
1. Mysql community edition
2. Works with python 2.7.2
3. Python Packages: mysql-connector, flask



# High Performance Ideas

*DATABASE*

To make the solution highly scalable, available and fast, we can use a distributed database system like a document store e.g. couchbase.

With a document store, we store the JSON blob and build indexes on top of GENE and HVGS fields. These database have ANSI SQL support and can query JSON.

A similar approach can be done by storing the data in redshift like columnar database with same schema structure as MySQL.

*WEBSERVER*

The code is written via flask which can be hosted on K8 pods behind a load balancer. 

The query interface is built out to interact with a common database which is expected to be scalable due to its distributed nature, as mentioned above