import unittest
from src.lib.exec_engine import ExecEngine

class TestIntegration(unittest.TestCase):

    def test_e2e_api(self):
        engine = ExecEngine()
        engine.destroy()
        engine.install()
        engine.load_data(file_path="test/test_data/test.tsv")
        print engine.query_gene('a', 0, 10)
        print engine.query_gene('A', 0, 10)
        print engine.query_hgvs('A', 0, 10)


    def test_e2e_api(self):
        engine = ExecEngine()
        engine.destroy()
        engine.install()
        engine.load_data(file_path="test/test_data/test.tsv")
        print engine.query_gene('a', 0, 10)
        print engine.query_gene('A', 0, 10)
        print engine.query_hgvs('A', 0, 10)

if __name__ == '__main__':
    unittest.main()