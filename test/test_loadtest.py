import unittest
from src.lib.exec_engine import ExecEngine

class TestLoadTest(unittest.TestCase):

    def test_e2e(self):
        engine = ExecEngine()
        engine.destroy()
        engine.install()
        engine.load_data(file_path="test/test_data/variant_results.tsv")
        engine.destroy()

if __name__ == '__main__':
    unittest.main()