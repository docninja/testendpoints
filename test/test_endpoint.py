import unittest
import os

class TestEndPoint(unittest.TestCase):

    def test_end_point_gene(self):
        comm="curl -d \"gene=a1&start_limit=0&end_limit=100\" -X POST http://127.0.0.1:8080/query_gene"
        self.command(comm)

    def test_end_point_hgvs(self):
        comm = "curl -d \"hgvs=a1&start_limit=0&end_limit=100\" -X POST http://127.0.0.1:8080/query_hgvs"
        self.command(comm)

    def command(self, command):
        os.system(command)

if __name__ == '__main__':
    unittest.main()