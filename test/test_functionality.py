import unittest
from src.lib.exec_engine import ExecEngine

class TestFunctionality(unittest.TestCase):

    def test_starts_any(self):
        engine = ExecEngine()
        engine.destroy()
        engine.install()
        engine.load_data(file_path="test/test_data/test.tsv")
        data= engine.query_gene('a', 0, 10)
        self.assertTrue(len(data) == 7)
        engine.destroy()

    def test_size_1(self):
        engine = ExecEngine()
        engine.destroy()
        engine.install()
        engine.load_data(file_path="test/test_data/test.tsv")
        data= engine.query_gene('a1', 0, 10)
        self.assertTrue(len(data) == 2)
        engine.destroy()

    def test_size_3(self):
        engine = ExecEngine()
        engine.destroy()
        engine.install()
        engine.load_data(file_path="test/test_data/test.tsv")
        data= engine.query_gene('aaa3', 0, 10)
        self.assertTrue(len(data) == 2)
        engine.destroy()

if __name__ == '__main__':
    unittest.main()