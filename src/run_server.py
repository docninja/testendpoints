from flask import Flask, url_for
from flask import request
from lib import logger
from lib import exec_engine
from lib import constants
import json


app = Flask(__name__)


@app.route('/')
def api_root():
    return 'Welcome to Test Server'


@app.route('/query_gene', methods=['POST'])
def query_gene():
	try:
		engine=exec_engine.ExecEngine()
		start_limit=request.form.get('start_limit',constants.START_LIMIT)
		end_limit = request.form.get('end_limit', constants.END_LIMIT)
		gene=request.form.get('gene','')
		data = engine.query_gene(gene, start_limit,end_limit)
		return json.dumps(data)
	except Exception, ex:
		logger.error_log.error(ex)
		raise


@app.route('/query_hgvs', methods=['POST'])
def query_hgvs():
	try:
		engine=exec_engine.ExecEngine()
		start_limit=request.form.get('start_limit',constants.START_LIMIT)
		end_limit = request.form.get('end_limit', constants.END_LIMIT)
		hgvs=request.form.get('hgvs','')
		data = engine.query_gene(hgvs, start_limit,end_limit)
		return json.dumps(data)
	except Exception, ex:
		logger.error_log.error(ex)
		raise

def run_server(port_num=8082):
	app.run(port=port_num)

if __name__ == '__main__':
    app.run(port=8101)