from environment import Environment
from db_exec_sql import DBExecSQL
import constants

class ExecEngine:
    def __init__(self):
        self.get_env()
        self.exec_sql=DBExecSQL(db_user=self.db_user,
                                db_name=constants.DB_DBNAME,
                                db_password=self.db_password,
                                db_host=self.db_host)

    def get_env(self):
        self.db_user=Environment.get_db_user()
        self.db_password = Environment.get_db_password()
        self.db_host = Environment.get_db_host()

    def install(self):
        self.exec_sql.db_install()

    def destroy(self):
        self.exec_sql.destroy_db()

    def load_data(self, file_path="data.tsv"):
        self.exec_sql.insert_data(file_path)

    def query_gene(self, gene, start_limit=constants.START_LIMIT, end_limit=constants.END_LIMIT):
        return self.exec_sql.search_gene(gene, start_limit=start_limit, end_limit=end_limit)

    def query_hgvs(self, hgvs, start_limit=constants.START_LIMIT, end_limit=constants.END_LIMIT):
        return self.exec_sql.search_hgvs(hgvs, start_limit=start_limit, end_limit=end_limit)
