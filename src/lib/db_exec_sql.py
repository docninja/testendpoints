from parser import Parser
from data_extract import DataExtract
import constants
import json
import logger
from dbutils import MySQLUtils
class DBExecSQL(object):
    def __init__(self, db_name=constants.DB_DBNAME, db_host=constants.DB_HOST,
                 db_user=constants.DB_USER, db_password= constants.DB_PASSWORD):
        self.sqlutil= MySQLUtils(db_name,db_host, db_user,db_password)

    def db_install(self):
        self.sqlutil.initialize_db()

    def destroy_db(self, schema=constants.DB_DBNAME):
        self.sqlutil.destroy_db(schema=schema)

    def insert_data(self, file_name):
        field_names=[constants.GENE_DATA_INDEX,constants.HGVS_DATA_INDEX]
        data=Parser.parse_file(file_name, field_names=field_names)
        for d in data:
            GENE=d[constants.GENE_DATA_INDEX]
            HGVS=d[constants.HGVS_DATA_INDEX]
            sql=constants.INSERT_SQL.format(GENE=GENE.lower(), HGVS=HGVS.lower(),
                                            JSON_BLOB=json.dumps(d[constants.JSOB_BLOB_DATA_INDEX]))
            try:
                self.sqlutil.insert_execute_query(sql)
            except Exception, ex:
                logger.error_log.error(ex)

    def search_gene(self, gene, start_limit=constants.START_LIMIT, end_limit=constants.END_LIMIT):
        sql=constants.SEARCH_GENE_SQL.format(GENE=gene.lower(), START_LIMIT=start_limit, END_LIMIT=end_limit)
        return self.__process_result(self.sqlutil.execute_query(sql))

    def search_hgvs(self, hgvs, start_limit=constants.START_LIMIT, end_limit=constants.END_LIMIT):
        sql=constants.SEARCH_HGVS_SQL.format(HGVS=hgvs.lower(), START_LIMIT=start_limit, END_LIMIT=end_limit)
        return self.__process_result(self.sqlutil.execute_query(sql))

    def __process_result(self, data):
        return [DataExtract.get_data(json.loads(str(d[constants.JSOB_BLOB_DB_FIELD_NAME]), strict=False)) for d in data]