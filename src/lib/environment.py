import os
import constants

class Environment:
    @staticmethod
    def get_db_host():
        return os.environ.get('DB_HOST', constants.DB_HOST)
    @staticmethod
    def get_db_password():
        return os.environ.get('DB_PASWORD', constants.DB_PASSWORD)
    @staticmethod
    def get_db_user():
        return os.environ.get('DB_USER', constants.DB_USER)
    @staticmethod
    def get_db_user():
        return os.environ.get('DB_USER', constants.DB_USER)