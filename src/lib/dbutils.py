import mysql.connector
import constants
import json
import logger
from mysql.connector import FieldType

class MySQLUtils(object):
    def __init__(self, database=constants.DB_DBNAME, host=constants.DB_HOST,
                 user_id=constants.DB_USER, password= constants.DB_PASSWORD):
        self.database=database
        self.password=password
        self.user_id=user_id
        self.host=host
        self._set_mysql_client_without_database(host=self.host, user_id=self.user_id, password=self.password)

    def initialize_db(self):
        self._db_execute_query(self._create_table_definition_query())


    def destroy_db(self, schema=constants.DB_DBNAME):
        self._db_execute_query(constants.DROP_SCHEMA_SQL.format(SCHEMA=schema))

    def insert_execute_query(self, query=""):
        logger.run_log.info(query)
        cur = self.mysql_connector_client.cursor()
        try:
            cur.execute(query)
            self.mysql_connector_client.commit()
        except Exception, ex:
            logger.error_log.error(ex)
            raise

    def execute_query(self, query=""):
        logger.run_log.info(query)
        cur = self.mysql_connector_client.cursor()
        try:
            cur.execute(query)
        except Exception, ex:
            logger.error_log.error(" ERROR running query : {}, {}".format(query, ex))

        rows = cur.fetchall()
        desc = cur.description
        columns = []
        data_set=[]
        for row in desc:
            columns.append({"column_name": row[0], "type": FieldType.get_info(row[1]).lower()})
        cur.close()
        for row in rows:
            ctr=0
            data={}
            for col in columns:
                data[col['column_name']] = row[ctr]
                ctr+=1
            data_set.append(data)
        return data_set

    def _create_table_definition_query(self, path=constants.DB_DEFINITION_FILE_PATH):
        sql = ""
        file = open(path)
        for line in file.readlines():
            sql += " " + line
        return sql

    def _reset_client_connection(self):
        self._close_mysql_connection()
        self._set_mysql_client(self.database, self.host, self.user_id, self.password)

    def _set_mysql_client(self, database="flightstats", host="127.0.0.1", user_id="root", password=""):
        self.mysql_connector_client = mysql.connector.connect(user=user_id, password=password, host=host, database=database)

    def _set_mysql_client_without_database(self, host="127.0.0.1", user_id="root", password=""):
        self.mysql_connector_client = mysql.connector.connect(user=user_id, password=password, host=host)

    def _close_mysql_connection(self):
        self.mysql_connector_client.close()

    def _db_execute_query(self, query=""):
        logger.run_log.info(query)
        cur = self.mysql_connector_client.cursor()
        try:
            rows = cur.execute(query, multi=True)
            for row in rows:
                logger.run_log.info(row)
        except Exception, ex:
            logger.error_log.error(ex)
            raise

