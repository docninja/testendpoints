import constants
class Parser:
    @staticmethod
    def parse_file(file_name, field_names=[]):
        fields=None
        data=[]
        with open(file_name) as file:
            for line in file.readlines():
                if not fields:
                    fields=Parser.extract_header_fields(line)
                else:
                    data.append(Parser.parse_line(fields, line, field_names))
        return data

    @staticmethod
    def extract_header_fields(line):
        return [field_name for field_name in line.split('\t')]

    @staticmethod
    def parse_line(fields,line, field_names=[]):
        data={}
        index=0
        for field_val in line.split('\t'):
            data[fields[index]]=field_val
            index+=1
        return Parser.create_dataset(data, field_names)

    @staticmethod
    def create_dataset(data, field_names=[]):
        d={}
        d[constants.JSOB_BLOB_DATA_INDEX]=data
        for field_name in field_names:
            d[field_name]=data[field_name]
        return d

