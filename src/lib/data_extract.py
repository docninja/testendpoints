class DataExtract:
    @staticmethod
    def get_data(data):
        d={}
        for k, v in data.iteritems():
            d[str(k)]=str(v)
        return d