DB_USER="root"
DB_PASSWORD=""
DB_HOST="localhost"
DB_DBNAME="TEST_SCHEMA"
CREATE_TABLE=""

INSERT_SQL="INSERT INTO TEST_SCHEMA.DATA_SET (GENE, HGVS, JSON_BLOB)" \
           "VALUES ('{GENE}','{HGVS}', '{JSON_BLOB}');"

SEARCH_GENE_SQL="SELECT JSON_BLOB from TEST_SCHEMA.DATA_SET USE INDEX (gene_index) " \
                "where GENE like '{GENE}%' LIMIT {START_LIMIT}, {END_LIMIT}"

SEARCH_GENE_LIKE_SQL="SELECT JSON_BLOB from TEST_SCHEMA.DATA_SET USE INDEX (geneindex) " \
                "where GENE LIKE '%{GENE}%' LIMIT {START_LIMIT}, {END_LIMIT}"

SEARCH_HGVS_SQL="SELECT JSON_BLOB from TEST_SCHEMA.DATA_SET USE INDEX (hgvs_index) " \
                "where HGVS like '{HGVS}%' LIMIT {START_LIMIT}, {END_LIMIT}"

SEARCH_HGVS_LIKE_SQL="SELECT JSON_BLOB from TEST_SCHEMA.DATA_SET USE INDEX (hgvs_index) " \
                "where HGVS = '%{HGVS}%' LIMIT {START_LIMIT}, {END_LIMIT}"

JSOB_BLOB_DATA_INDEX="JSON_BLOB"
GENE_DATA_INDEX="Gene"
HGVS_DATA_INDEX="Other Mappings"
HVFS_DB_FIELD_NAME="HVFS"
JSOB_BLOB_DB_FIELD_NAME="JSON_BLOB"
GENE_DB_FIELD_NAME="GENE"
START_LIMIT=0
END_LIMIT=10000

DROP_SCHEMA_SQL="DROP SCHEMA IF EXISTS {SCHEMA};"

DB_DEFINITION_FILE_PATH="config/table_definition.sql"

DATA_FIELDS="Gene	Nucleotide Change	Protein Change	Other Mappings	Alias	Transcripts	Region	Reported Classification	Inferred Classification	Source	Last Evaluated	Last Updated	URL	Submitter Comment	Assembly	Chr	Genomic Start	Genomic Stop	Ref	Alt	Accession	Reported Ref	Reported Alt"
