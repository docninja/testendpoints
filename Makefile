install:
	virtualenv venv
	venv/bin/pip install -r requirements.txt
	venv/bin/python destroy.py
	venv/bin/python install.py
	venv/bin/python -m unittest test.test_end_to_end
	ln -s $(shell pwd)/run.sh /usr/local/bin/start-server
	PATH=/usr/local/bin/myprogram:${PATH}
	export PATH=$(PATH)
	start-server
start-server:
	start-server --port=8080 &
stop-server:
	pkill python
clean:
	rm -rf venv
	rm *.log

